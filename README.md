# Vagrant Flask vm

Flask is a micro webdevelopment framework for Python. You are currently looking at the documentation of the development version.

Update

```bash
sudo apt-get update 
sudo apt-get upgrade -y
```
Install python-pip

```bash
sudo apt-get install python-pip
```
Install Flask

```bash
sudo pip install Flask
```
Once the install is complete, let's test it out

```bash
vi helloworld.py
```
Paste the following: (or use the ready to go py app in /vagrant directory)

```python
from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "Hello World!"

if __name__ == "__main__":
    app.run()
```

Exit vi.

# Run
Run the application,

```bash
python helloworld.py
```

Go to your browser and enter your ip, `http://your-ip-address:5000/`

If it worked you are all set!